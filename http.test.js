const { ajaxCall } = require('./http');
const { accountId, domains, headers } = require('./globalVariables');
const { objectHasOwnProertyFunc, typeOfProperty } = require('./util');

// verifico che l'account sia una string non vuota
test('test account', () => {
    const textTest = (text) => {
        if(typeof text === 'string' && text !== '') {
            return true;
        } else {
            return false;
        }
    }
    expect(textTest(accountId)).toBe(true);
});

// verifico che la get balance vada a buon fine
test('test get balance status', async () => {
    const balance = await ajaxCall('GET', domains + '/api/gbs/banking/v4.0/accounts/' + accountId + '/balance', {}, headers);
    expect(balance.status).toBe(200);
});

// verifico che la get balance restituisca le proprietà corrette
test('test get balance data', async () => {
    const property = ['availableBalance', 'balance', 'currency', 'date'];
    const balance = await ajaxCall('GET', domains + '/api/gbs/banking/v4.0/accounts/' + accountId + '/balance', {}, headers);
    expect(objectHasOwnProertyFunc(balance.data.payload, property)).toBe(true);
});

// verifico che la get balance abbia i tipo dei dati corretti
test('test get balance type of data', async () => {
    const typeProperty = {
        date: 'string',
        balance: 'number',
        availableBalance: 'number',
        currency: 'string'
    };
    const balance = await ajaxCall('GET', domains + '/api/gbs/banking/v4.0/accounts/' + accountId + '/balance', {}, headers);
    expect(typeOfProperty(balance.data.payload, typeProperty)).toBe(true);
});

// verifico che la get transactions vada a buon fine
test('test get transactions status', async () => {
    const transactions = await ajaxCall('GET', domains + '/api/gbs/banking/v4.0/accounts/' + accountId + '/transactions', {
        fromAccountingDate: '2019-01-01',
        toAccountingDate: '2019-12-01'
    }, headers);
    expect(transactions.status).toBe(200);
});

// verifico che la get transactions restituisca le proprietà corrette
test('test get transaction data', async () => {
    const property = ['transactionId', 'operationId', 'accountingDate', 'valueDate', 'type', 'amount', 'currency', 'description'];
    const transactions = await ajaxCall('GET', domains + '/api/gbs/banking/v4.0/accounts/' + accountId + '/transactions', {
        fromAccountingDate: '2019-01-01',
        toAccountingDate: '2019-12-01'
    }, headers);
    const arrayList = transactions.data.payload.list;
    const testProperty = (currentValue) => {
        return objectHasOwnProertyFunc(currentValue, property);
    };
    expect(arrayList.every(testProperty)).toBe(true);
});

// verifico che la get transactions abbia i tipo dei dati corretti
test('test get transaction type of data', async () => {
    const typeProperty = {
        transactionId: 'string',
        operationId: 'string',
        accountingDate: 'string',
        valueDate: 'string',
        type: 'object',
        amount: 'number',
        currency: 'string',
        description: 'string'
    };
    const transactions = await ajaxCall('GET', domains + '/api/gbs/banking/v4.0/accounts/' + accountId + '/transactions', {
        fromAccountingDate: '2019-01-01',
        toAccountingDate: '2019-12-01'
    }, headers);
    const arrayList = transactions.data.payload.list;
    const testProperty = (currentValue) => {
        return typeOfProperty(currentValue, typeProperty);
    };
    expect(arrayList.every(testProperty)).toBe(true);
});