import { ajaxCall, getBalance, getTransactions } from './http';
import { setFormatDate, setSymbolCurrrency } from './util';
import { domains, accountId, headers, cols } from './globalVariables';

$('document').ready(function () {

  $('.modal').modal();
  $('.overlay').show();
  $('#account-name').text(accountId);
  setPage();

  $('#paymentBtn').click(function(e) {
      const formPayment = $('#paymentForm').serializeArray();
      paymentsFunc(formPayment);
  });
});

const setPage = async () => {
  // instanzio i valori relativi all'account
  const balance = await getBalance();
  const dataBalanceCall = balance.data.payload;
  const curr = dataBalanceCall.currency;
  $('#date-account').text(setFormatDate(dataBalanceCall.date));
  $('#saldo-account').text(setSymbolCurrrency(curr) + ' ' + dataBalanceCall.balance);
  $('#saldoDisponibile-account').text(setSymbolCurrrency(curr) + ' ' + dataBalanceCall.availableBalance);

  // instanziamento griglia transazioni
  const transactions = await getTransactions();
  const dataTransactionsCall = transactions.data.payload.list;
  createTable(dataTransactionsCall);

  $('.overlay').hide();
}

// funzione payments
const paymentsFunc = async (paymentData) => {
  const urlPayments = domains + '/api/gbs/banking/v4.0/accounts/' + accountId + '/payments/money-transfers';
  const date = new Date();
  const day = date.getDay(date);
  const month = date.getMonth();
  const year = date.getFullYear();
  const fullDate = `0${day}/0${month}/${year}`;

  const dataPayments = {
      accountId: accountId,
      receiverName: paymentData[0].value,
      description: paymentData[1].value,
      currency: paymentData[3].value,
      amount: paymentData[2].value,
      executionDate: fullDate
  };
  try {
      const resultTransactions = await ajaxCall('GET', urlPayments, dataPayments, headers);
      console.log(resultTransactions);
  } catch (error) {
      console.log('transactions', error);
      alert("Errore codice: API000. Errore tecnico, La condizione BP049 non e' prevista per il conto id 14537780");
      $('.overlay').hide();
  }
}

// funzione di creazione della tabella transaizoni
const createTable = (data) => {
  createColumnTable();
  createRowTable(data);
}

// funzione di creazione delle colonne della tabella
const createColumnTable = () => {
  let colsHtml = '<tr>';
  for (let col of cols) {
      colsHtml += `<th data-field="${col.field}">${col.text}</th>`;
  }

  colsHtml += '</tr>';
  $('#transactionsTable thead').html(colsHtml);
}

// funzione di creazione delle righe della tabella
const createRowTable = (rows) => {
  let rowsHtml = '';
  for (let row of rows) {
      rowsHtml += `<tr>`;
      for (let col of cols) {

          switch (col.field) {
              case 'amount':
                  let amountClassColor = (Math.sign(row[col.field]) < 0) ? 'red-text' : 'green-text';
                  rowsHtml += `
                      <td class="${amountClassColor}">${row[col.field]} ${setSymbolCurrrency(row.currency)}</td>
                  `;
                  break;
              case 'type':
                  rowsHtml += `
                      <td>${row[col.field].enumeration}</td>
                  `;
                  break;
              case 'valueDate':
              case 'accountingDate':
                  rowsHtml += `
                      <td>${setFormatDate(row[col.field])}</td>
                  `;
                  break;
              default:
                  rowsHtml += `
                      <td>${row[col.field]}</td>
                  `;
                  break;
          }
      }
      rowsHtml += `</tr>`;
  }

  $('#transactionsTable tbody').html(rowsHtml);
}
