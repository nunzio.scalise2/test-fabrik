// funzione per lagestione degli errori
const errorHandler = (code) => {
    switch (code) {
        case 400:
            alert('Bad Request');
            break;
        case 503:
            alert('Servizio momentaneamente non disponible');
            break;
        default:
            alert('Errore generico');
            break;
    }
}

// converto la currency da tsto a simbolo
const setSymbolCurrrency = (curr) => {
    let symbol = '';
    switch (curr) {
        case 'EUR':
            symbol = '\u20AC';
            break;
        default:
            symbol = curr;
            break;
    }

    return symbol;
}

// cambio il formato della data in quello italiano
const setFormatDate = (data) => {
    const date = new Date(data)
    const dateTimeFormat = new Intl.DateTimeFormat('it-IT', {
        year: 'numeric',
        month: 'short',
        day: '2-digit'
    })
    const [{
        value: day
    }, , {
        value: month
    }, , {
        value: year
    }] = dateTimeFormat.formatToParts(date)

    return `${day}-${month.toUpperCase()}-${year}`;
}

const objectHasOwnProertyFunc = (data, property) => {
    let testProperty = true;
    for (let el of property) {
        if (!data.hasOwnProperty(el)) {
            return testProperty = false;
        } else {
            testProperty = true;
        }
    }

    return testProperty;
};

const typeOfProperty = (data, typeProperty) => {
    let testProperty = true;
    for (let el in data) {
        if(typeof data[el] === typeProperty[el]) {
            testProperty = true;
        } else {
            return testProperty = false;
        }
    }

    return testProperty;
};

// export { errorHandler, setSymbolCurrrency, setFormatDate};
exports.errorHandler = errorHandler;
exports.setSymbolCurrrency = setSymbolCurrrency;
exports.setFormatDate = setFormatDate;
exports.objectHasOwnProertyFunc = objectHasOwnProertyFunc;
exports.typeOfProperty = typeOfProperty;