// VARIABILI GLOBALI
const domains = 'https://sandbox.platfr.io';

const accountId = '14537780';

const headers = {
    'Auth-Schema': 'S2S',
    'Api-Key': 'FXOVVXXHVCPVPBZXIJOBGUGSKHDNFRRQJP',
};

const cols = [{
        field: 'operationId',
        text: 'Id Operazione'
    },
    {
        field: 'transactionId',
        text: 'Id Transazione'
    },
    {
        field: 'amount',
        text: 'Importo'
    },
    {
        field: 'description',
        text: 'Descrizione'
    },
    {
        field: 'type',
        text: 'Tipo Transazione'
    },
    {
        field: 'valueDate',
        text: 'Data Transazione'
    },
    {
        field: 'accountingDate',
        text: 'Data Acconto'
    },
];

// export { domains, accountId, headers, cols };
exports.domains = domains;
exports.accountId = accountId;
exports.headers = headers;
exports.cols = cols;