const {
  errorHandler
} = require('./util');
const {
  accountId,
  headers,
  domains
} = require('./globalVariables');
const axios = require('axios');

// funzione ajax generica 
const ajaxCall = (method, url, data, headers, dataType = 'json') => {
  let dataCall = {
    method: method,
    url: url,
    params: data,
    headers: headers,
    dataType: dataType
  };

  return axios(dataCall);
}

// get saldo account
const getBalance = () => {
  const urlBalance = domains + '/api/gbs/banking/v4.0/accounts/' + accountId + '/balance';
  const dataBalance = {};
  try {
    const resultBalance = ajaxCall('GET', urlBalance, dataBalance, headers);
    return resultBalance;

  } catch (error) {
    errorHandler(error.status);
    $('.overlay').hide();
  }
}

// get transazioni
const getTransactions = async () => {
  const urlTransactions = domains + '/api/gbs/banking/v4.0/accounts/' + accountId + '/transactions';
  const dataTransactions = {
    fromAccountingDate: '2019-01-01',
    toAccountingDate: '2019-12-01'
  };
  try {
    const resultTransactions = await ajaxCall('GET', urlTransactions, dataTransactions, headers);
    console.log(resultTransactions);
    return resultTransactions;
  } catch (error) {
    console.log('transactions', error);
    errorHandler(error.status);
  }
}

exports.ajaxCall = ajaxCall;
exports.getBalance = getBalance;
exports.getTransactions = getTransactions;
// export { ajaxCall, getBalance, getTransactions };